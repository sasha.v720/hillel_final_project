from django.urls import path

from guide import views
from guide.views import GuideListView, GuideDetailView, GuideCreateView, GuideItemCreateView

app_name = 'guide'

urlpatterns = [
    path('add', GuideCreateView.as_view(), name='create_guide'),
    path('add_item', GuideItemCreateView.as_view(), name='create_guide_item'),
    path('<int:guide_id>/', GuideDetailView.as_view(), name='guide'),
    path('<int:guide_id>/<int:character_id>', GuideDetailView.as_view(), name='guide'),
    path('', GuideListView.as_view(), name='guides_list'),
]