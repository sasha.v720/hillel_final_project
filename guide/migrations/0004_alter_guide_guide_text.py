# Generated by Django 3.2 on 2022-08-16 06:46

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('guide', '0003_auto_20220815_1007'),
    ]

    operations = [
        migrations.AlterField(
            model_name='guide',
            name='guide_text',
            field=ckeditor.fields.RichTextField(blank=True, null=True),
        ),
    ]
