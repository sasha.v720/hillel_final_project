from django import forms
from guide.models import GuideItem, Guide
from ckeditor.fields import RichTextField

class GuideItemForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Item name', 'class': 'form__inp__default'}))
    baseType = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'baseType', 'class': 'form__inp__default'}))
    ilvl = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder': 'ilvl', 'class': 'form__inp__default'}))
    icon = forms.ImageField(widget=forms.TextInput(attrs={'placeholder': 'Img URL', 'class': 'form__inp__default'}))
    implicitMods = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'implicitMods', 'class': 'form__inp__default'}))
    explicitMods = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'explicitMods', 'class': 'form__inp__default'}))
    utilityMods = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'utilityMods', 'class': 'form__inp__default'}))
    craftedMods = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'craftedMods', 'class': 'form__inp__default'}))
    enchantMods = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'enchantMods', 'class': 'form__inp__default'}))
    fracturedMods = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'fracturedMods', 'class': 'form__inp__default'}))


    class Meta:
        model = GuideItem
        fields = ('name', 'baseType', 'ilvl', 'icon', 'identified', 'implicitMods', 'explicitMods', 'utilityMods',
                  'craftedMods', 'enchantMods', 'fracturedMods')


class GuideForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Guide name', 'class': 'form__inp__default'}))
    league = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'League', 'class': 'form__inp__default'}))
    icon = forms.ImageField()
    guide_text = RichTextField()
    class Meta:
        model = Guide
        fields = ('name', 'league', 'guide_text', 'recommended_items')
