from django.contrib.auth.models import User
from django.db import models
from ckeditor.fields import RichTextField
import re


class GuideItem(models.Model):
    slug = models.SlugField(unique=True, allow_unicode=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    baseType = models.CharField(max_length=200)
    ilvl = models.CharField(max_length=100)
    icon = models.CharField(max_length=500,
                            default='https://web.poecdn.com/gen/image/WzI1LDE0LHsiZiI6IjJESXRlbXMvUmluZ3MvUmluZzYiLCJ3IjoxLCJoIjoxLCJzY2FsZSI6MX1d/74a5d719fd/Ring6.png')
    identified = models.BooleanField()
    implicitMods = models.TextField(blank=True, null=True)
    explicitMods = models.TextField(blank=True, null=True)
    utilityMods = models.TextField(blank=True, null=True)
    craftedMods = models.TextField(blank=True, null=True)
    enchantMods = models.TextField(blank=True, null=True)
    fracturedMods = models.TextField(blank=True, null=True)

    def __str__(self):
        return f' {self.name}||{self.baseType}'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.slug:
            if not self.name:
                self.slug = f'{self.baseType}'
            else:
                self.slug = f'{self.name} {self.baseType}'
        super().save(force_insert, force_update, using, update_fields)


class Guide(models.Model):
    name = models.CharField(max_length=500)
    league = models.CharField(max_length=500)
    guide_text = RichTextField(blank=True, null=True)
    icon = models.ImageField(blank=True, null=True, default='harvest-logo.jpg')
    creator = models.ForeignKey(User, null=True, blank=True, default=None, on_delete=models.SET_NULL)
    create_at = models.DateField(auto_now_add=True)
    recommended_items = models.ManyToManyField(GuideItem)

    def text_definition(self):
        self.guide_text = str(self.guide_text).replace('&nbsp;', ' ').replace('&#39;', "'")
        item_replace = re.findall('\$.*?\$', str(self.guide_text))

        for item in item_replace:
            name = re.findall('\$(.*)\$', item)
            try:
                item_from_build = GuideItem.objects.get(slug=name[0])
                self.guide_text = str(self.guide_text).replace(item, f'<div class="someitem"> {item_from_build.baseType}'
                                                                     f'<div class="someitem__hidden">'
                                                                     f'<img src="{item_from_build.icon}">'
                                                                     f'<p>Name : <span>{item_from_build.name}</span></p>'
                                                                     f'<p>Base type : <span>{item_from_build.baseType}</span>'
                                                                     f'</p><p>ItemLvL : <span>{item_from_build.ilvl}</span></p>'
                                                                     f'</div>'
                                                                     f'</div>')
            except:
                item_from_build = name[0]
                self.guide_text = str(self.guide_text).replace(item, f'<div class="someitem"> {item_from_build}'
                                                                     f'<div class="someitem__hidden">'
                                                                     f'<p>Name : <span>{item_from_build}</span></p>'
                                                                     f'</div>'
                                                                     f'</div>')
        return self.guide_text
