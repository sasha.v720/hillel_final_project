import requests
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect

# Create your views here.
from django.views.generic import ListView, DetailView, CreateView

from guide.forms import GuideForm, GuideItemForm
from guide.models import Guide, GuideItem
from user.models import Character, Item, Character_Class


# function for update heroes
def heroes_update(user):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, ke Gecko) Chrome/61.0.3163.100 Safari/537.36'}
    response = requests.get(f'https://www.pathofexile.com/character-window/get-characters?accountName={user.username}',
                            headers=headers)
    data = response.json()
    user_characters = Character.objects.filter(account=user)
    character_names = []
    for character in data:
        character_names.append(character['name'])
        if Character.objects.filter(name=character['name']):
            Character.objects.filter(name=character['name']).update(league=character['league'],
                                                                    level=character['level'])
        else:
            character_data = Character(
                name=character['name'],
                league=character['league'],
                character_class=Character_Class.objects.get(pk=character['classId']),
                level=character['level'],
                account=user
            )
            character_data.save()
    for character in user_characters:
        if character.name not in character_names:
            character.delete()


# function for update inventory
def update_inventory(character_id, username):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, ke Gecko) Chrome/61.0.3163.100 Safari/537.36'}
    response = requests.get(
        f'https://www.pathofexile.com/character-window/get-items?accountName={username}&character={Character.objects.get(pk=character_id).name}',
        headers=headers)
    data = response.json()
    updated_items = data['items']
    updated_items_id = [item_id['id'] for item_id in updated_items]
    chatacter_items = [item.item_id for item in list(Item.objects.filter(character_items=character_id))]
    for item in updated_items:
        if Item.objects.filter(item_id=item['id']):
            Item.objects.filter(item_id=item['id']).update(name=item.get('name'),
                                                           baseType=item.get('baseType'),
                                                           ilvl=item.get('ilvl'),
                                                           icon=item.get('icon'),
                                                           identified=item.get('identified'),
                                                           implicitMods=item.get('implicitMods'),
                                                           explicitMods=item.get('explicitMods'),
                                                           utilityMods=item.get('utilityMods'),
                                                           craftedMods=item.get('craftedMods'),
                                                           enchantMods=item.get('enchantMods'),
                                                           fracturedMods=item.get('fracturedMods'),
                                                           corrupted=item.get('corrupted'))
        else:
            item_data = Item(
                item_id=item.get('id'),
                name=item.get('name'),
                baseType=item.get('baseType'),
                ilvl=item.get('ilvl'),
                icon=item.get('icon'),
                identified=item.get('identified'),
                implicitMods=item.get('implicitMods'),
                explicitMods=item.get('explicitMods'),
                utilityMods=item.get('utilityMods'),
                craftedMods=item.get('craftedMods'),
                enchantMods=item.get('enchantMods'),
                fracturedMods=item.get('fracturedMods'),
                corrupted=item.get('corrupted'),
                character_items=Character.objects.get(pk=character_id))
            item_data.save()

    for item in chatacter_items:
        if item not in updated_items_id:
            item.delete()


class GuideItemCreateView(LoginRequiredMixin, CreateView):
    model = GuideItem
    form_class = GuideItemForm
    template_name = 'create_guide_item.html'
    login_url = 'user:login'

    def form_valid(self, form):
        guide_item = f'{self.request.POST.get("name")}'
        if GuideItem.objects.filter(name=guide_item):
            error = "Item alredy exist"
            return render(self.request, 'create_guide_item.html', context={'form': form, 'error': error})

        else:
            guide_item = f'{self.request.POST.get("baseType")}'
            if GuideItem.objects.filter(baseType=guide_item):
                error = "Item alredy exist"
                return render(self.request, 'create_guide_item.html', context={'form': form, 'error': error})
        form.save()
        return super().form_valid(form)


class GuideCreateView(LoginRequiredMixin, CreateView):
    model = Guide
    form_class = GuideForm
    template_name = 'create_guide.html'
    login_url = 'user:login'

    def form_valid(self, form):
        guide_name = self.request.POST.get("name")
        if Guide.objects.filter(name=guide_name):
            error = 'Guide already exist'
            return render(self.request, 'create_guide.html', context={'form': form, 'error': error})
        new_guide = form.save(commit=False)
        new_guide.creator = self.request.user
        new_guide.save()
        form.save_m2m()
        return super().form_valid(form)


class GuideListView(ListView):
    model = Guide
    ordering = ['-create_at']
    template_name = 'guides_list.html'


class GuideDetailView(LoginRequiredMixin, DetailView):
    model = Guide
    template_name = 'guide.html'
    login_url = 'user:login'
    pk_url_kwarg = 'guide_id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        guide_id = self.kwargs.get('guide_id')
        character_id = self.kwargs.get('character_id')
        guide = Guide.objects.get(pk=guide_id)
        user = self.request.user
        heroes_update(user)
        characters = Character.objects.filter(account=user)
        guide.text_definition()

        if character_id:
            self.template_name = 'guide_hero.html'
            guide_items = [item.name for item in guide.recommended_items.all()]
            update_inventory(character_id, self.request.user)
            character_items = list(Item.objects.filter(character_items=character_id))
            items = []
            for item in character_items:
                if item.name in guide_items:
                    items.append(item)
            context['guide'] = guide
            context['characters'] = characters
            context['items'] = items
            return context
        context['guide'] = guide
        context['characters'] = characters

        return context
