from django.contrib import admin

# Register your models here.
from user.models import Character, Item, Character_Class

admin.site.register(Character)
admin.site.register(Item)
admin.site.register(Character_Class)