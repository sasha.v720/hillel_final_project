from django import forms
from django.contrib.auth.forms import UserCreationForm, UsernameField
from django.contrib.auth.models import User


class NewUserCreationForm(UserCreationForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form__inp__default', 'placeholder': 'Username', 'type': 'text', 'autocomplete': 'off'}))
    first_name = forms.CharField(
        label="First Name",
        widget=forms.TextInput(attrs={'class': 'form__inp__default', 'placeholder': 'Firstname', 'type': 'text', 'autocomplete': 'off'}))
    last_name = forms.CharField(
        label="Last Name",
        widget=forms.TextInput(attrs={'class': 'form__inp__default', 'placeholder': 'Lastname', 'type': 'text', 'autocomplete': 'off'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form__inp__default', 'placeholder': 'Email', 'type': 'text', 'autocomplete': 'off'}))

    class Meta:
        model = User
        fields = ("username", "first_name", "last_name", "email")
