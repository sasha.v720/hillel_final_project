from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
import requests
# Create your views here.
from user.forms import NewUserCreationForm
from user.models import Character, Character_Class, Item


def signup_view(request):
    if request.method == 'POST':
        form = NewUserCreationForm(data=request.POST)
        if form.is_valid():
            form.save()
        return render(request, 'login.html', context={'form': form})
    form = NewUserCreationForm
    return render(request, 'signup.html', context={'form': form})


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            return redirect('guide:guides_list')
    form = AuthenticationForm()
    return render(request, 'login.html', context={'form': form})

def logout_view(request):
    logout(request)
    return redirect('user:login')



