from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Character_Class(models.Model):
    name = models.CharField(max_length=100)
    icon = models.CharField(max_length=500, default='https://static.wikia.nocookie.net/pathofexile_gamepedia/images/b/b1/Marauder_character_class.png/revision/latest?cb=20180311122932')


class Character(models.Model):
    name = models.CharField(max_length=100)
    league = models.CharField(max_length=100)
    character_class = models.ForeignKey(Character_Class, on_delete=models.CASCADE)
    level = models.IntegerField()
    account = models.ForeignKey(User, default=None, on_delete=models.CASCADE)


    def __str__(self):
        return f'{self.name}, {self.character_class.name}, {self.league}, {self.level}'


class Item(models.Model):
    item_id = models.CharField(max_length=500, default=None, blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    baseType = models.CharField(max_length=200)
    ilvl = models.CharField(max_length=100)
    icon = models.CharField(max_length=500, default='https://web.poecdn.com/gen/image/WzI1LDE0LHsiZiI6IjJESXRlbXMvUmluZ3MvUmluZzYiLCJ3IjoxLCJoIjoxLCJzY2FsZSI6MX1d/74a5d719fd/Ring6.png')
    identified = models.BooleanField()
    implicitMods = models.TextField(blank=True, null=True)
    explicitMods = models.TextField(blank=True, null=True)
    utilityMods = models.TextField(blank=True, null=True)
    craftedMods = models.TextField(blank=True, null=True)
    enchantMods = models.TextField(blank=True, null=True)
    fracturedMods = models.TextField(blank=True, null=True)
    corrupted = models.BooleanField(blank=True, null=True)
    character_items = models.ForeignKey(Character, default=None, on_delete=models.CASCADE)

    def __str__(self):
        return f' {self.name}||{self.baseType}'










